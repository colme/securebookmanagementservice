package com.appgate.bookmanagement.controller;

import com.appgate.bookmanagement.model.entity.Author;
import com.appgate.bookmanagement.model.repository.AuthorRepository;
import com.appgate.bookmanagement.service.dto.AuthorDTO;
import com.appgate.bookmanagement.service.mapper.AuthorMapper;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
class AuthorControllerTest {

    @Autowired
    private AuthorController authorController;

    @Mock
    private AuthorRepository authorRepository;


    @Autowired
    private AuthorMapper mapper;


    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Validate the query from all Authors and should return an HttpStatus 200
     */
    @Test
    public void getAllAuthorByTest(){

        List<Author> authorList = new ArrayList<>();
        Author author1 = new Author(1, "Oscar1", "Villarreal1", "0", "a@a.com");
        Author author2 = new Author(2, "Oscar2", "Villarreal2", "0", "a@a.com");
        Author author3 = new Author(3, "Oscar3", "Villarreal3", "0", "a@a.com");

        authorList.add(author1);
        authorList.add(author2);
        authorList.add(author3);

        Mockito.when(authorRepository.getAll()).thenReturn(authorList);

        //test
        ResponseEntity<List<AuthorDTO>> httpResponse = authorController.getAll();
        assertEquals(httpResponse.getStatusCode(), HttpStatus.OK);
    }

    /**
     * Validate the query of a Author by its id and an HttpStatus 200 is expected as a result
     */
    @Test
    public void authorByIdByTest(){
        Optional<Author> author = Optional.of(new Author(1, "Oscar1", "Villarreal1", "0", "a@a.com"));
        Mockito.when(authorRepository.authorById(1)).thenReturn(author);

        //test
        ResponseEntity<AuthorDTO> httpResponse = authorController.authorById(1);
        assertEquals(httpResponse.getStatusCode(), HttpStatus.OK);
    }

    /**
     * create a new Author and expect an HttpStatus 201 as a result
     */
    @Test
    public void saveAuthorTest(){
        Author author = new Author(1, "Oscar1", "Villarreal1", "0", "a@a.com");
        Mockito.when(authorRepository.save(new Author())).thenReturn(author);

        //test
        ResponseEntity<AuthorDTO> httpResponse = authorController.save(new AuthorDTO());
        assertEquals(httpResponse.getStatusCode(), HttpStatus.CREATED);
    }

    /**
     * Can't find the record and can't update, generates HttpStatus 404
     */
    @Test
    public void notUpdateAuthorTest(){
        Author authorIni = new Author(1, "Oscar1", "Villarreal1", "0", "a@a.com");
        Author authorEnd = new Author(1, "Oscar2", "Villarreal1", "0", "a@a.com");
        Mockito.when(authorRepository.update(authorIni)).thenReturn(authorEnd);

        //test
        ResponseEntity<AuthorDTO> httpResponse = authorController.update(mapper.toAuthorDTO(authorIni));
        assertEquals(httpResponse.getStatusCode(), HttpStatus.NOT_FOUND);
    }

    /**
     * A record of Author is deleted by its id, it must return a code 200
     */
    @Test
    public void deleteAuthorTest(){
        Optional<Author> author = Optional.of(new Author(1, "Oscar1", "Villarreal1", "0", "a@a.com"));
        Mockito.when(authorRepository.authorById(1)).thenReturn(author);

        //test
        ResponseEntity<AuthorDTO> httpResponse = authorController.delete(1);
        assertEquals(httpResponse.getStatusCode(), HttpStatus.OK);
    }


}