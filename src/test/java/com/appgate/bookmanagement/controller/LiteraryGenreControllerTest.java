package com.appgate.bookmanagement.controller;

import com.appgate.bookmanagement.model.entity.Genre;
import com.appgate.bookmanagement.model.repository.GenreRepository;
import com.appgate.bookmanagement.service.dto.GenreDTO;
import com.appgate.bookmanagement.service.mapper.LiteraryGenreMapper;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
class LiteraryGenreControllerTest {

    @Autowired
    private LiteraryGenreController literaryGenreController;

    @Mock
    private GenreRepository genreRepository;

    @Autowired
    private LiteraryGenreMapper mapper;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Validate the query from all Genres and should return an HttpStatus 200
     */
    @Test
    void getAllGenreTest() {
        List<Genre> genreList = new ArrayList<>();
        Genre genre1 = new Genre(1, "genre1", "genre1");
        Genre genre2 = new Genre(2, "genre2", "genre2");
        Genre genre3 = new Genre(3, "genre3", "genre3");

        genreList.add(genre1);
        genreList.add(genre2);
        genreList.add(genre3);

        Mockito.when(genreRepository.getAll()).thenReturn(genreList);

        //test
        ResponseEntity<List<GenreDTO>> httpResponse = literaryGenreController.getAll();
        assertEquals(httpResponse.getStatusCode(), HttpStatus.OK);
    }

    /**
     * Validate the query of a Genre by its id and an HttpStatus 200 is expected as a result
     */
    @Test
    void literaryGenreByIdTest() {
        Optional<Genre> genre = Optional.of( new Genre(1, "genre1", "genre1"));
        Mockito.when(genreRepository.genreById(1)).thenReturn(genre);

        //test
        ResponseEntity<GenreDTO> httpResponse = literaryGenreController.literaryGenreById(1);
        assertEquals(httpResponse.getStatusCode(), HttpStatus.OK);
    }

    /**
     * create a new Genre and expect an HttpStatus 201 as a result
     */
    @Test
    void saveGenreTest() {
        Genre genre = new Genre(1, "genre1", "genre1");
        Mockito.when(genreRepository.save(new Genre())).thenReturn(genre);

        //test
        ResponseEntity<GenreDTO> httpResponse = literaryGenreController.save(new GenreDTO());
        assertEquals(httpResponse.getStatusCode(), HttpStatus.CREATED);
    }

    /**
     * Can't find the record and can't update, generates HttpStatus 404
     */
    @Test
    void updateGenreTest() {
        Genre genreIni = new Genre(1, "genre1", "genre1");
        Genre genreEnd = new Genre(1, "genreNew", "genreNew");
        Mockito.when(genreRepository.update(genreIni)).thenReturn(genreEnd);

        //test
        ResponseEntity<GenreDTO> httpResponse = literaryGenreController.update(mapper.toGenreDTO(genreIni));
        assertEquals(httpResponse.getStatusCode(), HttpStatus.NOT_FOUND);
    }

    /**
     * A record of Genre is deleted by its id, it must return a code 200
     */
    @Test
    void deleteGenreTest() {
        Optional<Genre> genre = Optional.of(new Genre(1, "genre1", "genre1"));
        Mockito.when(genreRepository.genreById(1)).thenReturn(genre);

        //test
        ResponseEntity<GenreDTO> httpResponse = literaryGenreController.delete(1);
        assertEquals(httpResponse.getStatusCode(), HttpStatus.OK);
    }
}