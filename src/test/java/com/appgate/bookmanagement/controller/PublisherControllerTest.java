package com.appgate.bookmanagement.controller;

import com.appgate.bookmanagement.model.entity.Publisher;
import com.appgate.bookmanagement.model.repository.PublisherRepository;
import com.appgate.bookmanagement.service.PublisherServices;
import com.appgate.bookmanagement.service.dto.PublisherDTO;
import com.appgate.bookmanagement.service.mapper.PublisherMapper;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
class PublisherControllerTest {

    @Autowired
    private PublisherController publisherController;

    @Mock
    private PublisherRepository publisherRepository;

    @Mock
    private PublisherServices publisherServices;

    @Autowired
    private PublisherMapper mapper;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Validate the query from all publishers and should return an HttpStatus 200
     */
    @Test
    void getAllPublisherTest() {
        List<Publisher> publisherList = new ArrayList<>();
        Publisher publisher1 = new Publisher(1, "Alianza Distribuidora de Colombia", "Bogota", "6405858", "aliandis@.gmail.com", "http://aliandis@cable.net.co");
        Publisher publisher2 = new Publisher(2, "Asociación de Editoriales Universitarias de Colombia -ASEUC", "Bogota", "2904562", "aseuc@.gmail.com", "http://www.aseuc.org.co");
        Publisher publisher3 = new Publisher(3, "Cangrejo Editores", "Bogota", "3804010", "cangrejoeditores@.gmail.com", "http://cangrejoeditores.com");

        publisherList.add(publisher1);
        publisherList.add(publisher2);
        publisherList.add(publisher3);

        Mockito.when(publisherRepository.getAll()).thenReturn(publisherList);

        //test
        ResponseEntity<List<PublisherDTO>> httpResponse = publisherController.getAll();
        assertEquals(httpResponse.getStatusCode(), HttpStatus.OK);
    }

    /**
     * Validate the query of a publisher by its id and an HttpStatus 200 is expected as a result
     */
    @Test
    void publisherByIdTest() {
        Optional<Publisher> publisher = Optional.of( new Publisher(1, "Alianza Distribuidora de Colombia", "Bogota", "6405858", "aliandis@.gmail.com", "http://aliandis@cable.net.co"));
        Mockito.when(publisherRepository.publisherById(1)).thenReturn(publisher);

        //test
        ResponseEntity<PublisherDTO> httpResponse = publisherController.publisherById(1);
        assertEquals(httpResponse.getStatusCode(), HttpStatus.OK);
    }

    /**
     * create a new publisher and expect an HttpStatus 201 as a result
     */
    @Test
    void savePublisherTest() {
        Publisher publisher = new Publisher(1, "Alianza Distribuidora de Colombia", "Bogota", "6405858", "aliandis@.gmail.com", "http://aliandis@cable.net.co");
        Mockito.when(publisherRepository.save(new Publisher())).thenReturn(publisher);

        //test
        ResponseEntity<PublisherDTO> httpResponse = publisherController.save(new PublisherDTO());
        assertEquals(httpResponse.getStatusCode(), HttpStatus.CREATED);
    }

    /**
     * update a publisher and expect an HttpStatus 200 as a result
     */
    @Test
    void updatePublisherTest() {
        Publisher publisherIni = new Publisher(1, "Alianza Distribuidora de Colombia", "Bogota", "6405858", "aliandis@.gmail.com", "http://aliandis@cable.net.co");
        Publisher publisherEnd = new Publisher(1, "Alianza Distribuidora de Colombia", "Bogota", "6405858", "aliandis@.hotmail.com", "http://aliandis@cable.net.co");
        Mockito.when(publisherRepository.update(publisherIni)).thenReturn(publisherEnd);

        //test
        ResponseEntity<PublisherDTO> httpResponse = publisherController.update(mapper.toPublisherDTO(publisherIni));
        assertEquals(httpResponse.getStatusCode(), HttpStatus.OK);
    }
}