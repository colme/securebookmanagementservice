package com.appgate.bookmanagement.controller;

import com.appgate.bookmanagement.model.entity.Book;
import com.appgate.bookmanagement.model.repository.BookRepository;
import com.appgate.bookmanagement.service.dto.BookDTO;
import com.appgate.bookmanagement.service.dto.GenreDTO;
import com.appgate.bookmanagement.service.mapper.BookMapper;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
class BookControllerTest {

    @Autowired
    private BookController bookController;

    @Mock
    private BookRepository bookRepository;

    @Autowired
    private BookMapper mapper;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Validate the query from all Books and should return an HttpStatus 200
     */
    @Test
    void getAllBookTest() {
        List<Book> bookList = new ArrayList<>();
        Book Book1 = new Book(1, "Camino a Macondo", 2, 4, 4, 40000, LocalDate.now());
        Book Book2 = new Book(2, "El coronel no tiene quien le escriba", 2, 4, 4, 40000, LocalDate.now());
        Book Book3 = new Book(3, "Cien años de soledad", 2, 4, 4, 40000, LocalDate.now());

        bookList.add(Book1);
        bookList.add(Book2);
        bookList.add(Book3);

        Mockito.when(bookRepository.getAll()).thenReturn(bookList);

        //test
        ResponseEntity<List<BookDTO>> httpResponse = bookController.getAll();
        assertEquals(httpResponse.getStatusCode(), HttpStatus.OK);
    }

    /**
     * Validate the query of a Book by its id and an HttpStatus 200 is expected as a result
     */
    @Test
    void bookByIdTest() {
        Optional<Book> book = Optional.of( new Book(1, "Camino a Macondo", 2, 4, 4, 40000, LocalDate.now()));
        Mockito.when(bookRepository.bookById(1)).thenReturn(book);

        //test
        ResponseEntity<BookDTO> httpResponse = bookController.bookById(1);
        assertEquals(httpResponse.getStatusCode(), HttpStatus.OK);
    }

    /**
     * The record is found and updated, generates HttpStatus 200
     */
    @Test
    void updateBookTest() {
        Book bookIni = new Book(1, "Camino a Macondo", 2, 4, 4, 40000, LocalDate.now());
        Book bookEnd = new Book(1, "Camino a Macondo 2", 2, 4, 4, 40000, LocalDate.now());
        Mockito.when(bookRepository.update(bookIni)).thenReturn(bookEnd);

        //test
        ResponseEntity<GenreDTO> httpResponse = bookController.update(mapper.toBookDTO(bookIni));
        assertEquals(httpResponse.getStatusCode(), HttpStatus.OK);
    }

    /**
     * A record of Book is deleted by its id, it must return a code 200
     */
    @Test
    void deleteBookTest() {
        Optional<Book> book = Optional.of(new Book(1, "Camino a Macondo", 2, 4, 4, 40000, LocalDate.now()));
        Mockito.when(bookRepository.bookById(1)).thenReturn(book);

        //test
        ResponseEntity<BookDTO> httpResponse = bookController.delete(1);
        assertEquals(httpResponse.getStatusCode(), HttpStatus.OK);
    }

    /**
     * Consult books by their genre, return code HttpStatus 200
     */
    @Test
    void booksByLiteraryGenreId() {
        List<Book> bookList = new ArrayList<>();
        Book Book1 = new Book(1, "Camino a Macondo", 2, 4, 4, 40000, LocalDate.now());
        Book Book2 = new Book(2, "El coronel no tiene quien le escriba", 2, 4, 4, 40000, LocalDate.now());
        Book Book3 = new Book(3, "Cien años de soledad", 2, 4, 4, 40000, LocalDate.now());

        bookList.add(Book1);
        bookList.add(Book2);
        bookList.add(Book3);

        Mockito.when(bookRepository.BooksByGenreId(1)).thenReturn(bookList);

        //test
        ResponseEntity<List<BookDTO>> httpResponse = bookController.BooksByLiteraryGenreId(1);
        assertEquals(httpResponse.getStatusCode(), HttpStatus.OK);
    }

    /**
     * Consult books by their Publisher, return code HttpStatus 200
     */
    @Test
    void booksByPublisherId() {
        List<Book> bookList = new ArrayList<>();
        Book Book1 = new Book(1, "Camino a Macondo", 2, 4, 4, 40000, LocalDate.now());
        Book Book2 = new Book(2, "El coronel no tiene quien le escriba", 2, 4, 4, 40000, LocalDate.now());
        Book Book3 = new Book(3, "Cien años de soledad", 2, 4, 4, 40000, LocalDate.now());

        bookList.add(Book1);
        bookList.add(Book2);
        bookList.add(Book3);

        Mockito.when(bookRepository.BooksByPublisherId(1)).thenReturn(bookList);

        //test
        ResponseEntity<List<BookDTO>> httpResponse = bookController.BooksByPublisherId(1);
        assertEquals(httpResponse.getStatusCode(), HttpStatus.OK);
    }

    /**
     * Consult books by their Author, return code HttpStatus 200
     */
    @Test
    void booksByAuthorId() {
        List<Book> bookList = new ArrayList<>();
        Book Book1 = new Book(1, "Camino a Macondo", 2, 4, 4, 40000, LocalDate.now());
        Book Book2 = new Book(2, "El coronel no tiene quien le escriba", 2, 4, 4, 40000, LocalDate.now());
        Book Book3 = new Book(3, "Cien años de soledad", 2, 4, 4, 40000, LocalDate.now());

        bookList.add(Book1);
        bookList.add(Book2);
        bookList.add(Book3);

        Mockito.when(bookRepository.BooksByAuthorId(1)).thenReturn(bookList);

        //test
        ResponseEntity<List<BookDTO>> httpResponse = bookController.BooksByAuthorId(1);
        assertEquals(httpResponse.getStatusCode(), HttpStatus.OK);
    }
}