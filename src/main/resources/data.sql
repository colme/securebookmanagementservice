insert into authors (id_author, name, last_name, phone_number, email) values ('1', 'Oscar', 'Villarreal', '3116449874', 'a@a.com');
insert into authors (id_author, name, last_name, phone_number, email) values ('2', 'Alejandro', 'Colmenares', '2115448796', 'c@C.com');
insert into authors (id_author, name, last_name, phone_number, email) values ('3', 'Oscar Alejandro', 'Villarreal Colmenares', '3002115544', 'colme_11@colme.com');
insert into authors (id_author, name, last_name, phone_number, email) values ('4', 'Gabriel', 'Garcia Marquez', '', 'gabo@colme.com');

insert into genres (id_genre, name, description) values ('1', 'Epico', 'El diálogo y la descripción forman parte de la narrativa, de eventos reales o ficticios.  En la actualidad es el género narrativo.');
insert into genres (id_genre, name, description) values ('2', 'Lírico', 'Manifiesta lo emotivo interpretando lo real, utilizando elementos metafóricos.');
insert into genres (id_genre, name, description) values ('3', 'Dramático', 'Se desarrolla en función de episodios, con subtramas, donde el diálogo es fundamental.');
insert into genres (id_genre, name, description) values ('4', 'Didáctico', 'Su principal intención es enseñar, centrándose en lo que puede dejar lo narrado.');

insert into publishers (nit, name_publisher, address, phone_number, email, website) values ('1', 'Alianza Distribuidora de Colombia', 'Bogota', '6405858', 'aliandis@.gmail.com', 'http://aliandis@cable.net.co');
insert into publishers (nit, name_publisher, address, phone_number, email, website) values ('2', 'Asociación de Editoriales Universitarias de Colombia -ASEUC', 'Bogota', '2904562', 'aseuc@.gmail.com', 'http://www.aseuc.org.co');
insert into publishers (nit, name_publisher, address, phone_number, email, website) values ('3', 'Cangrejo Editores', 'Bogota', '3804010', 'cangrejoeditores@.gmail.com', 'http://cangrejoeditores.com');
insert into publishers (nit, name_publisher, address, phone_number, email, website) values ('4', 'Literatura Random House', 'Bogota', '3804010', 'cangrejoeditores@.gmail.com', 'http://cangrejoeditores.com');

insert into books (id_book, title, id_genre, id_author, id_publisher, total_price, date) values ('1', 'Camino a Macondo', 2, 4, 4, 40000, '2017-05-2');
insert into books (id_book, title, id_genre, id_author, id_publisher, total_price, date) values ('2', 'Crónica de una muerte anunciada', 2, 4, 3, 40000, '1981-05-2');
insert into books (id_book, title, id_genre, id_author, id_publisher, total_price, date) values ('3', 'El coronel no tiene quien le escriba', 2, 4, 2, 40000, '1961-05-2');
insert into books (id_book, title, id_genre, id_author, id_publisher, total_price, date) values ('4', 'Cien años de soledad', 2, 4, 1, 40000, '1967-05-2');