package com.appgate.bookmanagement.service;

import com.appgate.bookmanagement.service.dto.GenreDTO;
import com.appgate.bookmanagement.model.repository.IGenreRepository;
import com.appgate.bookmanagement.service.mapper.LiteraryGenreMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class LiteraryGenreService {

    @Autowired
    private IGenreRepository literaryGenreRepository;

    @Autowired
    private LiteraryGenreMapper mapper;

    /**
     * consult all literary genres
     * @return
     */
    public List<GenreDTO> getAll(){
        return mapper.toGenreDTOList(literaryGenreRepository.getAll());
    }

    /**
     * consult a genre by id
     * @param literaryGenreId
     * @return
     */
    public Optional<GenreDTO> literaryGenreById(int literaryGenreId){
        return literaryGenreRepository.genreById(literaryGenreId).map(genre -> mapper.toGenreDTO(genre));
    }

    /**
     * insert a new genre record
     * @param genreDTO
     * @return
     */
    public GenreDTO save(GenreDTO genreDTO){
        return mapper.toGenreDTO(literaryGenreRepository.save(mapper.toGenre(genreDTO)));
    }

    /**
     * update a new genre record
     * @param genreDTO
     * @return
     */
    public boolean update(GenreDTO genreDTO){
        return literaryGenreById(genreDTO.getLiteraryGenreId()).map(book -> {
            literaryGenreRepository.update(mapper.toGenre(genreDTO));
            return  true;
        }).orElse(false);
    }

    /**
     * delete a genre record by id
     * @param literaryGenreId
     */
    public boolean delete(int literaryGenreId){
        return literaryGenreById(literaryGenreId).map(book -> {
            literaryGenreRepository.delete(literaryGenreId);
            return  true;
        }).orElse(false);
    }
}
