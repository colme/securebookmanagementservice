package com.appgate.bookmanagement.service.mapper;

import com.appgate.bookmanagement.service.dto.AuthorDTO;
import com.appgate.bookmanagement.model.entity.Author;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface AuthorMapper {

    @Mappings({
            @Mapping(source = "idAuthor", target = "authorId"),
            @Mapping(source = "name", target = "nameAuthor"),
            @Mapping(source = "lastName", target = "lastNameAuthor"),
            @Mapping(source = "phoneNumber", target = "phoneNumberAuthor"),
            @Mapping(source = "eMail", target = "eMailAuthor"),
    })
    AuthorDTO toAuthorDTO(Author author);

    List<AuthorDTO> toAuthorDTOList(List<Author> Authors);

    @InheritInverseConfiguration
    @Mapping(target = "books", ignore = true)
    Author toAuthor(AuthorDTO authorDTO);
}
