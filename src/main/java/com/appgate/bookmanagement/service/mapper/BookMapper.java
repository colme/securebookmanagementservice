package com.appgate.bookmanagement.service.mapper;

import com.appgate.bookmanagement.service.dto.BookDTO;
import com.appgate.bookmanagement.model.entity.Book;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring", uses = {LiteraryGenreMapper.class, AuthorMapper.class, PublisherMapper.class} )
public interface BookMapper {

    @Mappings({
            @Mapping(source = "idBook", target = "bookId"),
            @Mapping(source = "title", target = "titleBook"),
            @Mapping(source = "idGenre", target = "literaryGenreId"),
            @Mapping(source = "idAuthor", target = "authorId"),
            @Mapping(source = "idPublisher", target = "publisherId"),
            @Mapping(source = "totalPrice", target = "priceBook"),
            @Mapping(source = "date", target = "publishedBook"),
            @Mapping(source = "genre", target = "genre"),
            @Mapping(source = "author", target = "author"),
            @Mapping(source = "publisher", target = "publisher"),
    })
    BookDTO toBookDTO(Book book);

    List<BookDTO> toBookDTOList(List<Book> books);

    @InheritInverseConfiguration
    Book toBook(BookDTO bookDTO);

}
