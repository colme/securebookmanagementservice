package com.appgate.bookmanagement.service.mapper;

import com.appgate.bookmanagement.service.dto.GenreDTO;
import com.appgate.bookmanagement.model.entity.Genre;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface LiteraryGenreMapper {

    @Mappings({
            @Mapping(source = "idGenre", target = "literaryGenreId"),
            @Mapping(source = "name", target = "nameGenre"),
            @Mapping(source = "description", target = "descriptionGenre"),
    })
    GenreDTO toGenreDTO(Genre genre);

    List<GenreDTO> toGenreDTOList(List<Genre> Genres);

    @InheritInverseConfiguration
    @Mapping(target = "books", ignore = true)
    Genre toGenre(GenreDTO genreDTO);

}
