package com.appgate.bookmanagement.service.mapper;

import com.appgate.bookmanagement.service.dto.PublisherDTO;
import com.appgate.bookmanagement.model.entity.Publisher;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PublisherMapper {

    @Mappings({
            @Mapping(source = "nit", target = "publisherId"),
            @Mapping(source = "namePublisher", target = "namePublisher"),
            @Mapping(source = "address", target = "addressPublisher"),
            @Mapping(source = "phoneNumber", target = "phoneNumberPublisher"),
            @Mapping(source = "eMail", target = "eMailPublisher"),
            @Mapping(source = "website", target = "websitePublisher"),
    })
    PublisherDTO toPublisherDTO(Publisher publisher);

    List<PublisherDTO> toPublisherDTOList(List<Publisher> editoriales);

    @InheritInverseConfiguration
    @Mapping(target = "books", ignore = true)
    Publisher toPublisher(PublisherDTO publisherDTO);

}
