package com.appgate.bookmanagement.service;

import com.appgate.bookmanagement.model.repository.IAuthorRepository;
import com.appgate.bookmanagement.service.dto.AuthorDTO;
import com.appgate.bookmanagement.model.repository.AuthorRepository;
import com.appgate.bookmanagement.service.mapper.AuthorMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AuthorService {

    @Autowired
    private IAuthorRepository authorRepository;

    @Autowired
    private AuthorMapper mapper;

    /**
     * consult all Authors
     * @return
     */
    public List<AuthorDTO> getAll(){
        return mapper.toAuthorDTOList(authorRepository.getAll());
    }

    /**
     * consult a author by id
     * @param authorId
     * @return
     */
    public Optional<AuthorDTO> authorById(int authorId){
        return authorRepository.authorById(authorId).map(author -> mapper.toAuthorDTO(author));
    }

    /**
     * insert a new author record
     * @param authorDTO
     * @return
     */
    public AuthorDTO save(AuthorDTO authorDTO){
        return mapper.toAuthorDTO(authorRepository.save(mapper.toAuthor(authorDTO)));
    }

    /**
     * update a new author record
     * @param authorDTO
     * @return
     */
    public boolean update(AuthorDTO authorDTO){
        return authorById(authorDTO.getAuthorId()).map(book -> {
            authorRepository.update(mapper.toAuthor(authorDTO));
            return  true;
        }).orElse(false);
    }

    /**
     * delete a author record by id
     * @param authorById
     */
    public boolean delete(int authorById){
        return authorById(authorById).map(book -> {
            authorRepository.delete(authorById);
            return  true;
        }).orElse(false);
    }

}
