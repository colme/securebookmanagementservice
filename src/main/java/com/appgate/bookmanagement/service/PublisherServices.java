package com.appgate.bookmanagement.service;

import com.appgate.bookmanagement.service.dto.PublisherDTO;
import com.appgate.bookmanagement.model.repository.IPublisherRepository;
import com.appgate.bookmanagement.service.mapper.PublisherMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PublisherServices {

    @Autowired
    private IPublisherRepository publisherRepository;

    @Autowired
    private PublisherMapper mapper;

    /**
     * consult all editorials
     * @return
     */
    public List<PublisherDTO> getAll(){
        return mapper.toPublisherDTOList(publisherRepository.getAll());
    }

    /**
     * consult a editorial by id
     * @param publisherId
     * @return
     */
    public Optional<PublisherDTO> publisherById(int publisherId){
        return publisherRepository.publisherById(publisherId).map(publisher -> mapper.toPublisherDTO(publisher));
    }

    /**
     * insert a new editorial record
     * @param publisherDTO
     * @return
     */
    public PublisherDTO save(PublisherDTO publisherDTO){
        return mapper.toPublisherDTO(publisherRepository.save(mapper.toPublisher(publisherDTO)));
    }

    /**
     * update a new editorial record
     * @param publisherDTO
     * @return
     */
    public boolean update(PublisherDTO publisherDTO){
        return publisherById(publisherDTO.getPublisherId()).map(book -> {
            publisherRepository.update(mapper.toPublisher(publisherDTO));
            return  true;
        }).orElse(false);
    }

    /**
     * delete a editorial record by id
     * @param publisherId
     */
    public boolean delete(int publisherId){
        return publisherById(publisherId).map(book -> {
            publisherRepository.delete(publisherId);
            return  true;
        }).orElse(false);
    }
}
