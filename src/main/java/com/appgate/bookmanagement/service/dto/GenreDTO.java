package com.appgate.bookmanagement.service.dto;

/**
 * Class literaryGenre exposed in our service complying with our Domain-Driven Design
 */
public class GenreDTO {

    /**
     * Primary key
     */
    private int literaryGenreId;

    /**
     * name of the literary genre
     */
    private String nameGenre;

    /**
     * description of the literary genre
     */
    private String descriptionGenre;

    public int getLiteraryGenreId() {
        return literaryGenreId;
    }

    public void setLiteraryGenreId(int literaryGenreId) {
        this.literaryGenreId = literaryGenreId;
    }

    public String getNameGenre() {
        return nameGenre;
    }

    public void setNameGenre(String nameGenre) {
        this.nameGenre = nameGenre;
    }

    public String getDescriptionGenre() {
        return descriptionGenre;
    }

    public void setDescriptionGenre(String descriptionGenre) {
        this.descriptionGenre = descriptionGenre;
    }
}
