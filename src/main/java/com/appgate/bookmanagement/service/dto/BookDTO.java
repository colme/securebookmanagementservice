package com.appgate.bookmanagement.service.dto;

import java.time.LocalDate;
import java.util.Date;

/**
 * Class Book exposed in our service complying with our Domain-Driven Design
 */
public class BookDTO {

    /**
     * Primary key
     */
    private int bookId;

    /**
     * name of the book
     */
    private String titleBook;

    /**
     * Id literary genre
     */
    private int literaryGenreId;

    /**
     * Id Author of the book
     */
    private int authorId;

    /**
     * Id Publisher of the book
     */
    private int publisherId;

    /**
     * price
     */
    private int priceBook;

    /**
     * publication date
     */
    private LocalDate publishedBook;

    /**
     * literary genre
     */
    private GenreDTO genre;

    /**
     * Author of the book
     */
    private AuthorDTO author;

    /**
     * Publisher of the book
     */
    private PublisherDTO publisher;

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public String getTitleBook() {
        return titleBook;
    }

    public void setTitleBook(String titleBook) {
        this.titleBook = titleBook;
    }

    public int getLiteraryGenreId() {
        return literaryGenreId;
    }

    public void setLiteraryGenreId(int literaryGenreId) {
        this.literaryGenreId = literaryGenreId;
    }

    public int getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }

    public int getPublisherId() {
        return publisherId;
    }

    public void setPublisherId(int publisherId) {
        this.publisherId = publisherId;
    }

    public int getPriceBook() {
        return priceBook;
    }

    public void setPriceBook(int priceBook) {
        this.priceBook = priceBook;
    }

    public LocalDate getPublishedBook() {
        return publishedBook;
    }

    public void setPublishedBook(LocalDate publishedBook) {
        this.publishedBook = publishedBook;
    }

    public GenreDTO getGenre() {
        return genre;
    }

    public void setGenre(GenreDTO genre) {
        this.genre = genre;
    }

    public AuthorDTO getAuthor() {
        return author;
    }

    public void setAuthor(AuthorDTO author) {
        this.author = author;
    }

    public PublisherDTO getPublisher() {
        return publisher;
    }

    public void setPublisher(PublisherDTO publisher) {
        this.publisher = publisher;
    }
}
