package com.appgate.bookmanagement.service.dto;

/**
 * Class Author exposed in our service complying with our Domain-Driven Design
 */
public class AuthorDTO {

    public AuthorDTO(){

    }

    /**
     * Primary key
     */
    private int authorId;

    /**
     * Author's name
     */
    private String nameAuthor;

    /**
     * Author's last name
     */
    private String lastNameAuthor;

    /**
     * telephone contact
     */
    private String phoneNumberAuthor;

    /**
     * e-mail contact
     */
    private String eMailAuthor;

    public int getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }

    public String getNameAuthor() {
        return nameAuthor;
    }

    public void setNameAuthor(String nameAuthor) {
        this.nameAuthor = nameAuthor;
    }

    public String getLastNameAuthor() {
        return lastNameAuthor;
    }

    public void setLastNameAuthor(String lastNameAuthor) {
        this.lastNameAuthor = lastNameAuthor;
    }

    public String getPhoneNumberAuthor() {
        return phoneNumberAuthor;
    }

    public void setPhoneNumberAuthor(String phoneNumberAuthor) {
        this.phoneNumberAuthor = phoneNumberAuthor;
    }

    public String geteMailAuthor() {
        return eMailAuthor;
    }

    public void seteMailAuthor(String eMailAuthor) {
        this.eMailAuthor = eMailAuthor;
    }
}
