package com.appgate.bookmanagement.service.dto;

/**
 * Class Publisher exposed in our service complying with our Domain-Driven Design
 */
public class PublisherDTO {

    /**
     * Primary key by NIT
     */
    private int publisherId;

    /**
     * Publisher's name
     */
    private String namePublisher;

    /**
     * address
     */
    private String addressPublisher;

    /**
     * telephone contact
     */
    private String phoneNumberPublisher;

    /**
     * e-mail contact
     */
    private String eMailPublisher;

    /**
     * website contact
     */
    private String websitePublisher;

    public int getPublisherId() {
        return publisherId;
    }

    public void setPublisherId(int publisherId) {
        this.publisherId = publisherId;
    }

    public String getNamePublisher() {
        return namePublisher;
    }

    public void setNamePublisher(String namePublisher) {
        this.namePublisher = namePublisher;
    }

    public String getAddressPublisher() {
        return addressPublisher;
    }

    public void setAddressPublisher(String addressPublisher) {
        this.addressPublisher = addressPublisher;
    }

    public String getPhoneNumberPublisher() {
        return phoneNumberPublisher;
    }

    public void setPhoneNumberPublisher(String phoneNumberPublisher) {
        this.phoneNumberPublisher = phoneNumberPublisher;
    }

    public String geteMailPublisher() {
        return eMailPublisher;
    }

    public void seteMailPublisher(String eMailPublisher) {
        this.eMailPublisher = eMailPublisher;
    }

    public String getWebsitePublisher() {
        return websitePublisher;
    }

    public void setWebsitePublisher(String websitePublisher) {
        this.websitePublisher = websitePublisher;
    }
}
