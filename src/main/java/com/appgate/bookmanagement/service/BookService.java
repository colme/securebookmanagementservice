package com.appgate.bookmanagement.service;

import com.appgate.bookmanagement.service.dto.BookDTO;
import com.appgate.bookmanagement.model.repository.IBookRepository;
import com.appgate.bookmanagement.service.mapper.BookMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * @author Oscar Villarreal
 */
@Service
public class BookService {

    @Autowired
    private IBookRepository bookRepository;

    @Autowired
    private BookMapper mapper;

    /**
     * create a new book record
     * @param bookDTO
     * @return
     */
    public BookDTO save(BookDTO bookDTO){
        return mapper.toBookDTO(bookRepository.save(mapper.toBook(bookDTO)));
    }

    /**
     * update a new book record
     * @param bookDTO
     * @return
     */
    public boolean update(BookDTO bookDTO){
        return bookById(bookDTO.getBookId()).map(book -> {
            bookRepository.update(mapper.toBook(bookDTO));
            return  true;
        }).orElse(false);
    }

    /**
     * consult a book by id
     * @param bookId
     * @return
     */
    public Optional<BookDTO> bookById(int bookId){
        return bookRepository.bookById(bookId).map(book -> mapper.toBookDTO(book));
    }

    /**
     * delete a book record by id
     * @param bookId
     */
    public boolean delete(int bookId){
        return bookById(bookId).map(book -> {
            bookRepository.delete(bookId);
            return  true;
        }).orElse(false);
    }

    /**
     * consult all books
     * @return
     */
    public List<BookDTO> getAll(){
        return mapper.toBookDTOList(bookRepository.getAll());
    }

    /**
     * check all books by genre
     * @param literaryGenreId
     * @return
     */
    public List<BookDTO> BooksByLiteraryGenreId(int literaryGenreId){
        return mapper.toBookDTOList(bookRepository.BooksByGenreId(literaryGenreId));
    }

    /**
     * check all books by editorial
     * @param publisherId
     * @return
     */
    public List<BookDTO> BooksByPublisherId(int publisherId){
        return mapper.toBookDTOList(bookRepository.BooksByPublisherId(publisherId));
    }

    /**
     * check all books by author
     * @param authorId
     * @return
     */
    public List<BookDTO> BooksByAuthorId(int authorId){
        return mapper.toBookDTOList(bookRepository.BooksByAuthorId(authorId));
    }
}
