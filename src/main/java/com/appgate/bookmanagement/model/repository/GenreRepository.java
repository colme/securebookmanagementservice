package com.appgate.bookmanagement.model.repository;

import com.appgate.bookmanagement.model.crud.GenreCrudRepository;
import com.appgate.bookmanagement.model.entity.Genre;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * This class interacts with the genres table of our database
 */
@Repository
public class GenreRepository implements IGenreRepository {

    @Autowired
    private GenreCrudRepository generoCrudRepository;

    @Override
    public List<Genre> getAll() {
        return (List<Genre>) generoCrudRepository.findAll();
    }

    @Override
    public Optional<Genre> genreById(int literaryGenreId) {
        return generoCrudRepository.findById(literaryGenreId);
    }

    @Override
    public Genre save(Genre genre) {
        return generoCrudRepository.save(genre);
    }

    @Override
    public Genre update(Genre genre) {
        return generoCrudRepository.save(genre);
    }

    @Override
    public void delete(int literaryGenreId) {
        generoCrudRepository.deleteById(literaryGenreId);
    }
}

