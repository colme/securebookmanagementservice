package com.appgate.bookmanagement.model.repository;

import com.appgate.bookmanagement.model.crud.AuthorCrudRepository;
import com.appgate.bookmanagement.model.entity.Author;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * This class interacts with the Authors table of our database
 */
@Repository
public class AuthorRepository implements IAuthorRepository {

    @Autowired
    private AuthorCrudRepository autorCrudRepository;

    @Override
    public List<Author> getAll() {
        return (List<Author>) autorCrudRepository.findAll();
    }

    @Override
    public Optional<Author> authorById(int authorId) {
        return autorCrudRepository.findById(authorId);
    }

    @Override
    public Author save(Author author) {
        return autorCrudRepository.save(author);
    }

    @Override
    public Author update(Author author) {
        return autorCrudRepository.save(author);
    }

    @Override
    public void delete(int authorId) {
        autorCrudRepository.deleteById(authorId);
    }
}
