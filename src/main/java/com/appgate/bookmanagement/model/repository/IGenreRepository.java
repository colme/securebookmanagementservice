package com.appgate.bookmanagement.model.repository;

import com.appgate.bookmanagement.model.entity.Genre;

import java.util.List;
import java.util.Optional;

public interface IGenreRepository {

    /**
     * consult genres
     * @return
     */
    List<Genre> getAll();

    /**
     * consult a genre by id
     * @param literaryGenreId
     * @return
     */
    Optional<Genre> genreById(int literaryGenreId);

    /**
     * insert a new genre record
     * @param genre
     * @return
     */
    Genre save(Genre genre);

    /**
     * update a new genre record
     * @param genre
     * @return
     */
    Genre update(Genre genre);

    /**
     * delete a genre record by id
     * @param literaryGenreId
     */
    void delete(int literaryGenreId);
}
