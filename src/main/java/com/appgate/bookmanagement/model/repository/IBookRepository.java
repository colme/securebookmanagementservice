package com.appgate.bookmanagement.model.repository;

import com.appgate.bookmanagement.model.entity.Book;

import java.util.List;
import java.util.Optional;

public interface IBookRepository {

    /**
     * insert a new book record
     * @param book
     * @return
     */
    Book save(Book book);

    /**
     * update a new book record
     * @param book
     * @return
     */
    Book update(Book book);

    /**
     * delete a book record by id
     * @param bookId
     */
    void delete(int bookId);

    /**
     * consult a book by id
     * @param bookId
     * @return
     */
    Optional<Book> bookById(int bookId);

    /**
     * consult books
     * @return
     */
    List<Book> getAll();

    /**
     * check all books by genre
     * @param literaryGenreId
     * @return
     */
    List<Book> BooksByGenreId(int literaryGenreId);

    /**
     * check all books by editorial
     * @param publisherId
     * @return
     */
    List<Book> BooksByPublisherId(int publisherId);

    /**
     * check all books by author
     * @param authorId
     * @return
     */
    List<Book> BooksByAuthorId(int authorId);
}
