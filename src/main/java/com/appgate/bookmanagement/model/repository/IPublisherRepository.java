package com.appgate.bookmanagement.model.repository;

import com.appgate.bookmanagement.model.entity.Publisher;

import java.util.List;
import java.util.Optional;

public interface IPublisherRepository {

    /**
     * consult all editorials
     * @return
     */
    List<Publisher> getAll();

    /**
     * consult a editorial by id
     * @param publisherId
     * @return
     */
    Optional<Publisher> publisherById(int publisherId);

    /**
     * insert a new editorial record
     * @param publisher
     * @return
     */
    Publisher save(Publisher publisher);

    /**
     * update a new editorial record
     * @param publisher
     * @return
     */
    Publisher update(Publisher publisher);

    /**
     * delete a editorial record by id
     * @param publisherId
     */
    void delete(int publisherId);
}
