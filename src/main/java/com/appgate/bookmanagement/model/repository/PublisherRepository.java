package com.appgate.bookmanagement.model.repository;

import com.appgate.bookmanagement.model.crud.PublisherCrudRepositoy;
import com.appgate.bookmanagement.model.entity.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * This class interacts with the Editorials table of our database
 */
@Repository
public class PublisherRepository implements IPublisherRepository {

    @Autowired
    private PublisherCrudRepositoy editorialCrudRepositoy;

    @Override
    public List<Publisher> getAll() {
        return (List<Publisher>) editorialCrudRepositoy.findAll();
    }

    @Override
    public Optional<Publisher> publisherById(int publisherId) {
        return editorialCrudRepositoy.findById(publisherId);
    }

    @Override
    public Publisher save(Publisher publisher) {
        return editorialCrudRepositoy.save(publisher);
    }

    @Override
    public Publisher update(Publisher publisher) {
        return editorialCrudRepositoy.save(publisher);
    }

    @Override
    public void delete(int publisherId) {
        editorialCrudRepositoy.deleteById(publisherId);
    }
}
