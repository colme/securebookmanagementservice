package com.appgate.bookmanagement.model.repository;

import com.appgate.bookmanagement.model.crud.BookCrudRepository;
import com.appgate.bookmanagement.model.entity.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * This class interacts with the books table of our database
 */
@Repository
public class BookRepository implements IBookRepository {

    @Autowired
    private BookCrudRepository libroCrudRepository;

    @Override
    public Book save(Book book) {
        return libroCrudRepository.save(book);
    }

    @Override
    public Book update(Book book) {
        return libroCrudRepository.save(book);
    }

    @Override
    public void delete(int bookId) {
        libroCrudRepository.deleteById(bookId);
    }

    @Override
    public Optional<Book> bookById(int bookId) {
        return libroCrudRepository.findById(bookId);
    }

    @Override
    public List<Book> getAll() {
        return (List<Book>) libroCrudRepository.findAll();
    }

    @Override
    public List<Book> BooksByGenreId(int literaryGenreId) {
        return libroCrudRepository.findByGenre_IdGenreOrderByTitleAsc(literaryGenreId);
    }

    @Override
    public List<Book> BooksByPublisherId(int publisherId) {
        return libroCrudRepository.findByPublisher_NitOrderByTitleAsc(publisherId);
    }

    @Override
    public List<Book> BooksByAuthorId(int authorId) {
        return libroCrudRepository.findByAuthor_IdAuthorOrderByTitleAsc(authorId);
    }
}
