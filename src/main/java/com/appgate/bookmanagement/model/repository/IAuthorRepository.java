package com.appgate.bookmanagement.model.repository;

import com.appgate.bookmanagement.model.entity.Author;

import java.util.List;
import java.util.Optional;

public interface IAuthorRepository {

    /**
     * consult Authors
     * @return
     */
    List<Author> getAll();

    /**
     * consult a author by id
     * @param authorId
     * @return
     */
    Optional<Author> authorById(int authorId);

    /**
     * insert a new author record
     * @param author
     * @return
     */
    Author save(Author author);

    /**
     * update a new author record
     * @param author
     * @return
     */
    Author update(Author author);

    /**
     * delete a author record by id
     * @param authorId
     */
    void delete(int authorId);
}
