package com.appgate.bookmanagement.model.crud;

import com.appgate.bookmanagement.model.entity.Publisher;
import org.springframework.data.repository.CrudRepository;

public interface PublisherCrudRepositoy extends CrudRepository<Publisher, Integer> {
}
