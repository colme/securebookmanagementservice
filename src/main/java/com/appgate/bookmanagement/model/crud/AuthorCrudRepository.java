package com.appgate.bookmanagement.model.crud;

import com.appgate.bookmanagement.model.entity.Author;
import org.springframework.data.repository.CrudRepository;

public interface AuthorCrudRepository extends CrudRepository<Author, Integer> {
}
