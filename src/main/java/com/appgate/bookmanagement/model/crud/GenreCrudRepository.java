package com.appgate.bookmanagement.model.crud;

import com.appgate.bookmanagement.model.entity.Genre;
import org.springframework.data.repository.CrudRepository;

public interface GenreCrudRepository extends CrudRepository<Genre, Integer> {
}
