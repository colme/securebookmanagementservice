package com.appgate.bookmanagement.model.crud;

import com.appgate.bookmanagement.model.entity.Book;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BookCrudRepository extends CrudRepository<Book, Integer>{

    List<Book> findByPublisher_NitOrderByTitleAsc(int nit);

    List<Book> findByAuthor_IdAuthorOrderByTitleAsc(int idAuthor);

    List<Book> findByGenre_IdGenreOrderByTitleAsc(int idGenre);
}
