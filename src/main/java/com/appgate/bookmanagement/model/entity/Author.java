package com.appgate.bookmanagement.model.entity;

import javax.persistence.*;
import java.util.List;

/**
 * Authors table that records their main information
 */
@Entity
@Table(name = "authors")
public class Author {

    public Author(){

    }

    public Author(Integer idAuthor, String name, String lastName, String phoneNumber, String eMail) {
        this.idAuthor = idAuthor;
        this.name = name;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.eMail = eMail;
    }

    /**
     * Primary key
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_author")
    private Integer idAuthor;

    /**
     * Author's name
     */
    private String name;

    /**
     * Author's last name
     */
    @Column(name = "last_name")
    private String lastName;

    /**
     * telephone contact
     */
    @Column(name = "phone_number")
    private String phoneNumber;

    /**
     * e-mail contact
     */
    @Column(name = "email")
    private String eMail;

    /**
     * Books written by the author
     */
    @OneToMany(mappedBy = "author")
    private List<Book> books;

    public Integer getIdAuthor() {
        return idAuthor;
    }

    public void setIdAuthor(Integer idAuthor) {
        this.idAuthor = idAuthor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String geteMail() {
        return eMail;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }
}
