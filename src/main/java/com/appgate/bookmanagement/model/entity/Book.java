package com.appgate.bookmanagement.model.entity;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;

/**
 * Books table that records their main information
 */
@Entity
@Table(name = "books")
public class Book {

    public Book(){

    }

    public Book(Integer idBook, String title, Integer idGenre, Integer idAuthor, Integer idPublisher, int totalPrice, LocalDate date) {
        this.idBook = idBook;
        this.title = title;
        this.idGenre = idGenre;
        this.idAuthor = idAuthor;
        this.idPublisher = idPublisher;
        this.totalPrice = totalPrice;
        this.date = date;
    }

    /**
     * Primary key
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_book")
    private Integer idBook;

    /**
     * name of the book
     */
    private String title;

    /**
     * Id literary genre
     */
    @Column(name = "id_genre")
    private Integer idGenre;

    /**
     * Id author of the book
     */
    @Column(name = "id_author")
    private Integer idAuthor;

    /**
     * Id editorial
     */
    @Column(name = "id_publisher")
    private Integer idPublisher;

    /**
     * price
     */
    @Column(name = "total_price")
    private int totalPrice;

    /**
     * publication date
     */
    private LocalDate date;

    /**
     * literary genre
     */
    @ManyToOne
    @JoinColumn(name = "id_genre", insertable = false, updatable = false)
    private Genre genre;

    /**
     * Author of the book
     */
    @ManyToOne
    @JoinColumn(name = "id_author", insertable = false, updatable = false)
    private Author author;

    /**
     * Editorial
     */
    @ManyToOne
    @JoinColumn(name = "id_publisher", insertable = false, updatable = false)
    private Publisher publisher;

    public Integer getIdBook() {
        return idBook;
    }

    public void setIdBook(Integer idBook) {
        this.idBook = idBook;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getIdGenre() {
        return idGenre;
    }

    public void setIdGenre(Integer idGenre) {
        this.idGenre = idGenre;
    }

    public Integer getIdAuthor() {
        return idAuthor;
    }

    public void setIdAuthor(Integer idAuthor) {
        this.idAuthor = idAuthor;
    }

    public Integer getIdPublisher() {
        return idPublisher;
    }

    public void setIdPublisher(Integer idPublisher) {
        this.idPublisher = idPublisher;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public Publisher getPublisher() {
        return publisher;
    }

    public void setPublisher(Publisher publisher) {
        this.publisher = publisher;
    }
}
