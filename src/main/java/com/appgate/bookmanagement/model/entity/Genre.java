package com.appgate.bookmanagement.model.entity;

import javax.persistence.*;
import java.util.List;

/**
 * Books genders that records their main information
 */
@Entity
@Table(name = "genres")
public class Genre {

    public Genre(){

    }

    public Genre(Integer idGenre, String name, String description) {
        this.idGenre = idGenre;
        this.name = name;
        this.description = description;
    }

    /**
     * Primary key
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_genre")
    private Integer idGenre;

    /**
     * name
     */
    private String name;

    /**
     * description of the literary genre
     */
    private String description;

    /**
     * books of a specific genre
     */
    @OneToMany(mappedBy = "genre")
    private List<Book> books;

    public Integer getIdGenre() {
        return idGenre;
    }

    public void setIdGenre(Integer idGenre) {
        this.idGenre = idGenre;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }
}
