package com.appgate.bookmanagement.model.entity;

import javax.persistence.*;
import java.util.List;

/**
 * Books Editorials that records their main information
 */
@Entity
@Table(name = "publishers")
public class Publisher {

    public Publisher(){

    }

    public Publisher(Integer nit, String namePublisher, String address, String phoneNumber, String eMail, String website) {
        this.nit = nit;
        this.namePublisher = namePublisher;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.eMail = eMail;
        this.website = website;
    }

    /**
     * Primary key by NIT
     */
    @Id
    private Integer nit;

    /**
     * Editorial's name
     */
    @Column(name = "name_publisher")
    private String namePublisher;

    /**
     * address
     */
    private String address;

    /**
     * telephone contact
     */
    @Column(name = "phone_number")
    private String phoneNumber;

    /**
     * e-mail contact
     */
    @Column(name = "email")
    private String eMail;

    /**
     * website contact
     */
    private String website;

    /**
     * Books by the Editorial
     */
    @OneToMany(mappedBy = "publisher")
    private List<Book> books;

    public Integer getNit() {
        return nit;
    }

    public void setNit(Integer nit) {
        this.nit = nit;
    }

    public String getNamePublisher() {
        return namePublisher;
    }

    public void setNamePublisher(String namePublisher) {
        this.namePublisher = namePublisher;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String geteMail() {
        return eMail;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }
}
