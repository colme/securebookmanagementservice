package com.appgate.bookmanagement.controller;

import com.appgate.bookmanagement.service.dto.GenreDTO;
import com.appgate.bookmanagement.service.LiteraryGenreService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/genres")
public class LiteraryGenreController {

    @Autowired
    private LiteraryGenreService literaryGenreService;

    @GetMapping
    @ApiOperation("Get Literary genres")
    @ApiResponse(code = 200, message = "OK")
    public ResponseEntity<List<GenreDTO>> getAll(){
        return new ResponseEntity<>(literaryGenreService.getAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @ApiOperation("Search a literary genre with an ID")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "Literary genre not found")
    })
    public ResponseEntity<GenreDTO> literaryGenreById(
            @ApiParam(value = "The id of the literary genre", required = true)
            @PathVariable("id") int literaryGenreId){
        return literaryGenreService.literaryGenreById(literaryGenreId)
                .map(author -> new ResponseEntity<>(author, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping
    @ApiOperation("save a literary genre")
    @ApiResponse(code = 201, message = "CREATED")
    public ResponseEntity<GenreDTO> save(@RequestBody GenreDTO genreDTO){
        return new ResponseEntity<>(literaryGenreService.save(genreDTO), HttpStatus.CREATED);
    }

    @PatchMapping
    @ApiOperation("update a literary genre")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "Literary genre not found")
    })
    public ResponseEntity update(@RequestBody GenreDTO genreDTO){
        if(literaryGenreService.update(genreDTO)){
            return new ResponseEntity(HttpStatus.OK);
        }else{
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    @ApiOperation("delete a literary genre with an ID")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "Literary genre not found")
    })
    public ResponseEntity delete(@PathVariable("id") int literaryGenreId){
        if(literaryGenreService.delete(literaryGenreId)){
            return new ResponseEntity(HttpStatus.OK);
        }else{
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

}
