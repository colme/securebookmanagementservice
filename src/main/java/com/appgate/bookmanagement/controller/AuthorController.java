package com.appgate.bookmanagement.controller;

import com.appgate.bookmanagement.service.dto.AuthorDTO;
import com.appgate.bookmanagement.service.AuthorService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/authors")
public class AuthorController {

    @Autowired
    private AuthorService authorService;

    @GetMapping
    @ApiOperation("Get authors")
    @ApiResponse(code = 200, message = "OK")
    public ResponseEntity<List<AuthorDTO>> getAll(){
        return new ResponseEntity<>(authorService.getAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @ApiOperation("Search an author with an ID")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "Author not found")
    })
    public ResponseEntity<AuthorDTO> authorById(
            @ApiParam(value = "The id of the author", required = true)
            @PathVariable("id") int authorId){
        return authorService.authorById(authorId)
                .map(author -> new ResponseEntity<>(author, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping
    @ApiOperation("save an author")
    @ApiResponse(code = 201, message = "CREATED")
    public ResponseEntity<AuthorDTO> save(@RequestBody AuthorDTO authorDTO){
        return new ResponseEntity<>(authorService.save(authorDTO), HttpStatus.CREATED);
    }

    @PatchMapping
    @ApiOperation("update an author")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "Author not found")
    })
    public ResponseEntity update(@RequestBody AuthorDTO authorDTO){
        if(authorService.update(authorDTO)){
            return new ResponseEntity(HttpStatus.OK);
        }else{
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    @ApiOperation("delete an author with an ID")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "Author not found")
    })
    public ResponseEntity delete(@PathVariable("id") int authorById){
        if(authorService.delete(authorById)){
            return new ResponseEntity(HttpStatus.OK);
        }else{
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }
}
