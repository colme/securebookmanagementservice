package com.appgate.bookmanagement.controller;

import com.appgate.bookmanagement.service.dto.BookDTO;
import com.appgate.bookmanagement.service.BookService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/books")
public class BookController {

    @Autowired
    public BookService bookService;

    @GetMapping
    @ApiOperation("Get books")
    @ApiResponse(code = 200, message = "OK")
    public ResponseEntity<List<BookDTO>> getAll(){
        return new ResponseEntity<>(bookService.getAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @ApiOperation("Search a book with an ID")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "Book not found")
    })
    public ResponseEntity<BookDTO> bookById(
            @ApiParam(value = "The id of the book", required = true)
            @PathVariable("id")int bookId){
        return bookService.bookById(bookId)
                .map(author -> new ResponseEntity<>(author, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping
    @ApiOperation("save a book")
    @ApiResponse(code = 201, message = "CREATED")
    public ResponseEntity<BookDTO> save(@RequestBody BookDTO bookDTO){
        return new ResponseEntity<>(bookService.save(bookDTO), HttpStatus.CREATED);
    }

    @PatchMapping
    @ApiOperation("update a book")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "Book not found")
    })
    public ResponseEntity update(@RequestBody BookDTO bookDTO){
        if(bookService.update(bookDTO)){
            return new ResponseEntity(HttpStatus.OK);
        }else{
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    @ApiOperation("delete a book with an ID")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "Book not found")
    })
    public ResponseEntity delete(@PathVariable("id") int bookId){
        if(bookService.delete(bookId)){
            return new ResponseEntity(HttpStatus.OK);
        }else{
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/literaryGenre/{literaryGenreId}")
    @ApiOperation("Search all book with a literary genre Id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "Book not found")
    })
    public ResponseEntity<List<BookDTO>>BooksByLiteraryGenreId(
            @ApiParam(value = "The id of the literary genre", required = true)
            @PathVariable("literaryGenreId") int literaryGenreId){
        return new ResponseEntity<>(bookService.BooksByLiteraryGenreId(literaryGenreId), HttpStatus.OK);
    }

    @GetMapping("/publisher/{publisherId}")
    @ApiOperation("Search all book with a publisher Id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "Book not found")
    })
    public ResponseEntity<List<BookDTO>> BooksByPublisherId(
            @ApiParam(value = "The id of the publisher", required = true)
            @PathVariable("publisherId") int publisherId){
        return new ResponseEntity<>(bookService.BooksByPublisherId(publisherId), HttpStatus.OK);
    }

    @GetMapping("/author/{authorId}")
    @ApiOperation("Search all book with an author Id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "Book not found")
    })
    public ResponseEntity<List<BookDTO>> BooksByAuthorId(
            @ApiParam(value = "The id of the author", required = true)
            @PathVariable("authorId") int authorId){
        return new ResponseEntity<>(bookService.BooksByAuthorId(authorId), HttpStatus.OK);
    }
}
