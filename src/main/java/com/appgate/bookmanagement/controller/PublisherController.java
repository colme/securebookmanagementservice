package com.appgate.bookmanagement.controller;

import com.appgate.bookmanagement.service.dto.PublisherDTO;
import com.appgate.bookmanagement.service.PublisherServices;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/publishers")
public class PublisherController {

    @Autowired
    private PublisherServices publisherServices;

    @GetMapping
    @ApiOperation("Get publishers")
    @ApiResponse(code = 200, message = "OK")
    public ResponseEntity<List<PublisherDTO>> getAll(){
        return new ResponseEntity<>(publisherServices.getAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @ApiOperation("Search a publisher with an ID")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "Publisher not found")
    })
    public ResponseEntity<PublisherDTO> publisherById(
            @ApiParam(value = "The id of the publisher", required = true)
            @PathVariable("id") int publisherId){
        return publisherServices.publisherById(publisherId)
                .map(author -> new ResponseEntity<>(author, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping
    @ApiOperation("save a publisher")
    @ApiResponse(code = 201, message = "CREATED")
    public ResponseEntity<PublisherDTO> save(@RequestBody PublisherDTO publisherDTO){
        return new ResponseEntity<>(publisherServices.save(publisherDTO), HttpStatus.CREATED);
    }

    @PatchMapping
    @ApiOperation("update a publisher")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "Publisher not found")
    })
    public ResponseEntity update(@RequestBody PublisherDTO publisherDTO){
        if(publisherServices.update(publisherDTO)){
            return new ResponseEntity(HttpStatus.OK);
        }else{
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    @ApiOperation("delete a publisher with an ID")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "Publisher not found")
    })
    public ResponseEntity delete(@PathVariable("id") int publisherId){
        if(publisherServices.delete(publisherId)){
            return new ResponseEntity(HttpStatus.OK);
        }else{
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }
}
