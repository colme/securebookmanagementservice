# appgate

### Secure Book Management Service

Este servicio fue desarrollado con arquitectura REST, una segunda opción era con arquitectura SOA

El proyecto fue desarrollado con el framework SpringBoot, para el repositorio de librerías trabajamos con Maven y como servidor de aplicaciones usamos el servidor que viene embebido con SpringBoot (Servidor Tomcat).

Este desarrollo se baso en el estándar REST. Usando springboot para tomar ventaja de autoconfiguración e inyección de dependencias

La persistencia fue trabajada con Spring Data y nuestra base de datos con H2. El sistema carga una data inicial al ser desplegada

Los datos para ingresar a la base de datos son los siguientes

http://localhost:8090/book-management/api/h2

JDBC URL: jdbc:h2:mem:appgateDB

user name: sa

password:




Para brindarle seguridad al modelo de datos, se implemente la aplicación orientada al Dominio y la transformación de las tablas entre la capa de servicio y de persistencia se trabajo con mapstruct

El servicio se encuentra documentado con swagger2 y puede consultar todo el catalogo del servicio en:

http://localhost:8090/book-management/api/swagger-ui.html